void main() {
  //Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012;
  // a) Sort and print the apps by name;
  // b) Print the winning app of 2017 and the winning app of 2018.;
  // c) the Print total number of apps from the array.

  List<MTNApp> mtnApps = [
    MTNApp(name: "takealot", year: "2013"),
    MTNApp(name: "vodacom", year: "2017"),
    MTNApp(name: "FNB", year: "2021"),
    MTNApp(name: "Boxer", year: "2016"),
    MTNApp(name: "Spar", year: "2018"),
    MTNApp(name: "TUT", year: "2018")
  ];

  mtnApps.sort((a, b) => a.name!.compareTo(b.name!));

  print(mtnApps);

  for (MTNApp app in mtnApps) {
    if (app.year == "2017" || app.year == "2018") {
      print(app);
    }
  }

  int totalApp = mtnApps.length;

  print("The total number of apps is $totalApp");
}

class MTNApp {
  String? name;
  String? year;

//constructor
  MTNApp({this.name, this.year});

  @override
  String toString() => "name: $name, year: $year";
}

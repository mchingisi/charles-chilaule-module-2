void main() {
//Create a class and
//a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards.
//b) Create a function inside the class, transform the app name to all capital letters and then print the output.

  List<MTNApp> mtnApps = [
    MTNApp(
        name: "takealot", year: "2013", category: "shopping", developer: "Ben"),
    MTNApp(
        name: "vodacom", year: "2017", category: "netwoking", developer: "Bin"),
    MTNApp(name: "FNB", year: "2021", category: "Money", developer: "Polo"),
    MTNApp(name: "Boxer", year: "2016", category: "B", developer: "Rich"),
    MTNApp(name: "Spar", year: "2018", category: "G", developer: "Zerk"),
    MTNApp(name: "TUT", year: "2018", category: "F", developer: "Lucia")
  ];

  for (int i = 0; i < mtnApps.length; i++) {
    MTNApp app = mtnApps[i];
    //print(app);
    app.printData();
  }

  for (MTNApp app in mtnApps) {
    app.toUpper();
  }
}

class MTNApp {
  String? name;
  String? year;
  String? category;
  String? developer;

//constructor
  MTNApp({this.name, this.year, this.category, this.developer});

  void printData() {
    print(
        "The name of the is $name and category is $category and the developer is $developer and it won the award on $year");
  }

  void toUpper() {
    String appName = name!.toUpperCase();
    print(appName);
  }
}
